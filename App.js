import React, {useState, Component, createContext, useEffect} from 'react'
import { View, Text, Image, TextInput, TouchableOpacity, Button, ScrollView, FlatList, processColor} from 'react-native'
import Intro from './src/screens/Tugas1/Intro.js'
import Profile from './src/screens/Tugas2/Profile.js'
import TodoList from './src/screens/Tugas3/TodoList.js'
import NewTodoList from './src/screens/Tugas4'
import Routes from './src/config/Routes'
import Login from './src/screens/Login'
import firebase from '@react-native-firebase/app'
import Register from './src/screens/Register'
import BottomNavBar from './src/component/BottomNavBar'
import Home from './src/screens/Home'



var firebaseConfig = {
  apiKey: "AIzaSyAan0H2Dp5DUjw3kPX_uQLVxstv6rO5SUE",
  authDomain: "rizalsanbercodeapp-22ec7.firebaseapp.com",
  databaseURL: "https://rizalsanbercodeapp-22ec7.firebaseio.com",
  projectId: "rizalsanbercodeapp-22ec7",
  storageBucket: "rizalsanbercodeapp-22ec7.appspot.com",
  messagingSenderId: "494274952026",
  appId: "1:494274952026:web:2e411ce8e8bb9cc6f05163",
  measurementId: "G-T477XS0D8N"
};

if(!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig)
}

export default function App() {

  return(
    <View style={{flex: 1}}>
      <Routes />
    </View>
  )
}

const Tugas1 = () => {
  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Intro />
    </View>
  )
}

const Tugas2 = () => {
  return (
      <Profile />
  )
}

const Tugas3 = () => {
  return (
      <TodoList />
  )
}

const Tugas4 = () => {
  return (
    <NewTodoList />
  )
}






        
