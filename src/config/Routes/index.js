import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import SplashScreen from './../../screens/Splashscreen';
import Intro from './../../screens/Intro';
import Login from './../../screens/Login';
import Profile from './../../screens/Tugas2/Profile.js'
import Home from './../../screens/Home';
import Map from './../../screens/Map'
import Chart from '../../screens/Chart'
import Chat from './../../screens/Chat'
import Asynstorage from '@react-native-community/async-storage'
import BlankScreen from '../../screens/BlankScreen'
const Stack = createStackNavigator();

const MainNavigation = () => (
    <Stack.Navigator>
        <Stack.Screen name="Blank" component={BlankScreen} options={{ headerShown: false }} />
        <Stack.Screen name="Intro" component={Intro} options={{ headerShown: false }} />
        <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
        <Stack.Screen name="Home" component={Home} options={{ headerShown: false }} />
        <Stack.Screen name="Profile" component={Profile} options={{ headerShown: false}} />
        <Stack.Screen name="Map" component={Map} options={{ headerShown: false }} />
        <Stack.Screen name="Chart" component={Chart} options={{ headerShown: false }} />
        <Stack.Screen name="Chat" component={Chat} options={{ headerShown: false }} />
        
    </Stack.Navigator>
)

function AppNavigation() {
    const [ isLoading, setIsLoading ] = React.useState(true)

    //mengatur durasi splashscreen saat aplikasi pertama kali dibuka
    React.useEffect(() => {
        setTimeout(() => {
            setIsLoading(!isLoading)
        }, 3000)
    }, [])

    if(isLoading) {
        return <SplashScreen />
    }

    return (
        <NavigationContainer>
            <MainNavigation />
        </NavigationContainer>
    )
}

export default AppNavigation;