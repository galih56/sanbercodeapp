import React from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'

export default function index(props) {
    return (
        <View style={{flexDirection: 'row', justifyContent: 'space-around', width: '100%', height: 70, position: 'absolute', bottom: 0, backgroundColor: '#FFFFFF'}}>
            <TouchableOpacity onPress={() => props.navigation.navigate('Home')}>
            <View style={{alignItems: 'center', justifyContent: 'center'}}>
                <Image source={require('./../../assets/icons/home.png')}  style={{width: 40, height: 40}}/>
                <Text>Home</Text>
            </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => props.navigation.navigate('Map')}>
            <View style={{alignItems: 'center', justifyContent: 'center'}}>
                <Image source={require('./../../assets/icons/map.png')} style={{width: 40, height: 40}}/>
                <Text>Map</Text>
            </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => props.navigation.navigate('Chat')}>
            <View style={{alignItems: 'center', justifyContent: 'center'}}>
                <Image source={require('./../../assets/icons/chat.png')} style={{width: 40, height: 40}}/>
                <Text>Chat</Text>
            </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => props.navigation.navigate('Profile')}>
            <View style={{alignItems: 'center', justifyContent: 'center'}}>
                <Image source={require('./../../assets/icons/home.png')} style={{width: 40, height: 40}}/>
                <Text>Profile</Text>
            </View>
            </TouchableOpacity>
      </View>
    )
}
