import React from 'react'
import { View, Text, Image, ScrollView } from 'react-native'
import BottomNavBar from './../../component/BottomNavBar'
import { TouchableOpacity } from 'react-native-gesture-handler'

export default function index({ navigation }) {
    return (
        <View style={{flex: 1}}>
            <ScrollView style={{flex: 1, padding:10, marginBottom: 70 }}>
        <View style={{width: '100%', height: 140, marginVertical: 5}}>
          <View style={{backgroundColor: '#088dc4', paddingLeft: 10, paddingTop:5, borderTopLeftRadius: 10, borderTopRightRadius: 10}}>
            <Text style={{fontWeight:'bold', color: '#FFFFFF', height: 30, fontSize: 15}}>Kelas</Text>
          </View>
          <View style={{backgroundColor: '#3EC6FF', flex: 1, flexDirection: 'row', justifyContent: 'space-around', borderBottomStartRadius: 10, borderBottomEndRadius: 10}}>
            <View style={{alignItems: 'center', justifyContent: 'center'}}>
            <TouchableOpacity onPress={() => navigation.navigate('Chart', {name: 'Rizal'})} style={{alignItems: 'center', justifyContent: 'center'}}>
              <Image source={require('./../../assets/icons/react.png')} style={{width: 50, height: 50}}/>
              <Text style={{fontSize: 12, fontWeight: 'bold', color: '#FFFFFF'}}>React Native</Text>
              </TouchableOpacity>
            </View>
            
            
            <View style={{alignItems: 'center', justifyContent: 'center'}}>
              <Image source={require('./../../assets/icons/python.png')} style={{width: 50, height: 50}}/>
              <Text style={{fontSize: 12, fontWeight: 'bold', color: '#FFFFFF'}}>Data Science</Text>
            </View>
            <View style={{alignItems: 'center', justifyContent: 'center'}}>
              <Image source={require('./../../assets/icons/react.png')} style={{width: 50, height: 50}}/>
              <Text style={{fontSize: 12, fontWeight: 'bold', color: '#FFFFFF'}}>React JS</Text>
            </View>
            <View style={{alignItems: 'center', justifyContent: 'center'}}>
              <Image source={require('./../../assets/icons/laravel.png')} style={{width: 50, height: 50}}/>
              <Text style={{fontSize: 12, fontWeight: 'bold', color: '#FFFFFF'}}>Laravel</Text>
            </View>
            
            
          </View>

        </View>
        <View style={{width: '100%', height: 140, marginVertical: 5}}>
          <View style={{backgroundColor: '#088dc4', paddingLeft: 10, paddingTop:5, borderTopLeftRadius: 10, borderTopRightRadius: 10}}>
            <Text style={{fontWeight:'bold', color: '#FFFFFF', height: 30, fontSize: 15}}>Kelas</Text>
          </View>
          <View style={{backgroundColor: '#3EC6FF', flex: 1, flexDirection: 'row', justifyContent: 'space-around', borderBottomStartRadius: 10, borderBottomEndRadius: 10}}>
            <View style={{alignItems: 'center', justifyContent: 'center'}}>
              <Image source={require('./../../assets/icons/wordpress.png')} style={{width: 50, height: 50}}/>
              <Text style={{fontSize: 12, fontWeight: 'bold', color: '#FFFFFF'}}>Wordpress</Text>
            </View>
            <View style={{alignItems: 'center', justifyContent: 'center'}}>
              <Image source={require('./../../assets/icons/website.png')} style={{width: 50, height: 50}}/>
              <Text style={{fontSize: 12, fontWeight: 'bold', color: '#FFFFFF'}}>Desain Grafis</Text>
            </View>
            <View style={{alignItems: 'center', justifyContent: 'center'}}>
              <Image source={require('./../../assets/icons/server.png')} style={{width: 50, height: 50}}/>
              <Text style={{fontSize: 12, fontWeight: 'bold', color: '#FFFFFF'}}>Web Server</Text>
            </View>
            <View style={{alignItems: 'center', justifyContent: 'center'}}>
              <Image source={require('./../../assets/icons/ux.png')} style={{width: 50, height: 50}}/>
              <Text style={{fontSize: 12, fontWeight: 'bold', color: '#FFFFFF'}}>UI/UX Design</Text>
            </View>
          </View>
        </View>
        <View style={{marginVertical: 5}}>
          <View style={{width: '100%'}}>
            <View style={{backgroundColor: '#088dc4', paddingLeft: 10, paddingTop:5, borderTopLeftRadius: 10, borderTopRightRadius: 10}}>
              <Text style={{fontWeight:'bold', color: '#FFFFFF', height: 30, fontSize: 15}}>Summary</Text>
            </View>
            <View style={{backgroundColor: '#3EC6FF', paddingLeft: 10, paddingTop:5}}>
              <Text style={{fontWeight:'bold', color: '#FFFFFF', height: 30, fontSize: 15}}>React Native</Text>
            </View>
            <View style={{backgroundColor: '#088dc4', paddingLeft: 10, paddingTop:5, flexDirection: 'row', justifyContent:'space-around'}}>
              <View>
                <Text style={{fontWeight:'bold', color: '#FFFFFF', height: 30, fontSize: 15}}>Today</Text>
                <Text style={{fontWeight:'bold', color: '#FFFFFF', height: 30, fontSize: 15}}>Total</Text>
              </View>
              <View>
                <Text style={{fontWeight:'bold', color: '#FFFFFF', height: 30, fontSize: 15}}>20 Orang</Text>
                <Text style={{fontWeight:'bold', color: '#FFFFFF', height: 30, fontSize: 15}}>100 Orang</Text>
              </View>
            </View>
            <View style={{backgroundColor: '#3EC6FF', paddingLeft: 10, paddingTop:5}}>
              <Text style={{fontWeight:'bold', color: '#FFFFFF', height: 30, fontSize: 15}}>Data Science</Text>
            </View>
            <View style={{backgroundColor: '#088dc4', paddingLeft: 10, paddingTop:5, flexDirection: 'row', justifyContent:'space-around'}}>
              <View>
                <Text style={{fontWeight:'bold', color: '#FFFFFF', height: 30, fontSize: 15}}>Today</Text>
                <Text style={{fontWeight:'bold', color: '#FFFFFF', height: 30, fontSize: 15}}>Total</Text>
              </View>
              <View>
                <Text style={{fontWeight:'bold', color: '#FFFFFF', height: 30, fontSize: 15}}>20 Orang</Text>
                <Text style={{fontWeight:'bold', color: '#FFFFFF', height: 30, fontSize: 15}}>100 Orang</Text>
              </View>
            </View>
            <View style={{backgroundColor: '#3EC6FF', paddingLeft: 10, paddingTop:5}}>
              <Text style={{fontWeight:'bold', color: '#FFFFFF', height: 30, fontSize: 15}}>React Js</Text>
            </View>
            <View style={{backgroundColor: '#088dc4', paddingLeft: 10, paddingTop:5, flexDirection: 'row', justifyContent:'space-around'}}>
              <View>
                <Text style={{fontWeight:'bold', color: '#FFFFFF', height: 30, fontSize: 15}}>Today</Text>
                <Text style={{fontWeight:'bold', color: '#FFFFFF', height: 30, fontSize: 15}}>Total</Text>
              </View>
              <View>
                <Text style={{fontWeight:'bold', color: '#FFFFFF', height: 30, fontSize: 15}}>20 Orang</Text>
                <Text style={{fontWeight:'bold', color: '#FFFFFF', height: 30, fontSize: 15}}>100 Orang</Text>
              </View>
            </View>
            <View style={{backgroundColor: '#3EC6FF', paddingLeft: 10, paddingTop:5}}>
              <Text style={{fontWeight:'bold', color: '#FFFFFF', height: 30, fontSize: 15}}>Laravel</Text>
            </View>
            <View style={{backgroundColor: '#088dc4', paddingLeft: 10, paddingTop:5, flexDirection: 'row', justifyContent:'space-around'}}>
              <View>
                <Text style={{fontWeight:'bold', color: '#FFFFFF', height: 30, fontSize: 15}}>Today</Text>
                <Text style={{fontWeight:'bold', color: '#FFFFFF', height: 30, fontSize: 15}}>Total</Text>
              </View>
              <View>
                <Text style={{fontWeight:'bold', color: '#FFFFFF', height: 30, fontSize: 15}}>20 Orang</Text>
                <Text style={{fontWeight:'bold', color: '#FFFFFF', height: 30, fontSize: 15}}>100 Orang</Text>
              </View>
            </View>
            <View style={{backgroundColor: '#3EC6FF', paddingLeft: 10, paddingTop:5}}>
              <Text style={{fontWeight:'bold', color: '#FFFFFF', height: 30, fontSize: 15}}>Word Press</Text>
            </View>
            <View style={{backgroundColor: '#088dc4', paddingLeft: 10, paddingTop:5, flexDirection: 'row', justifyContent:'space-around'}}>
              <View>
                <Text style={{fontWeight:'bold', color: '#FFFFFF', height: 30, fontSize: 15}}>Today</Text>
                <Text style={{fontWeight:'bold', color: '#FFFFFF', height: 30, fontSize: 15}}>Total</Text>
              </View>
              <View>
                <Text style={{fontWeight:'bold', color: '#FFFFFF', height: 30, fontSize: 15}}>20 Orang</Text>
                <Text style={{fontWeight:'bold', color: '#FFFFFF', height: 30, fontSize: 15}}>100 Orang</Text>
              </View>
            </View>
          </View>
        </View>
      </ScrollView>
      <BottomNavBar navigation={navigation} />
    </View>
    )
}
