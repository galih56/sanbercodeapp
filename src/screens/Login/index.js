import React, {useEffect, useState} from 'react';
import {
    View,
    Text,
    Image,
    Button,
    TextInput,
    StatusBar,
    TouchableOpacity,
} from 'react-native';
// import styles from './style';
import Axios from 'axios'
import Asynstorage from '@react-native-community/async-storage'
import api from './../../config/api'
import auth from '@react-native-firebase/auth'
import TouchID from 'react-native-touch-id'
import { GoogleSignin, statusCodes, GoogleSigninButton } from '@react-native-community/google-signin'
import { CommonActions } from '@react-navigation/native'

const config = {
    title: 'Authentication Required',
    imageColor : '#191970',
    imageErrorColor : 'red',
    sensorDescription : 'Touch Sensor',
    sensorErrorDescription : 'Failed',
    cancelText: 'Cancel'
}

function Login({ navigation }) {

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    const saveToken = async (token) => {
        try {
            await Asynstorage.setItem('token', token)
        } catch (err) {
            console.log(err)
        }
    }

    useEffect(() => {
        configureGoogleSignin()
    })

    const configureGoogleSignin = () =>{
        GoogleSignin.configure({
            offlineAccess: false,
            webClientId: '494274952026-hgenlpsjbihugpommv1l1mm06rspbd14.apps.googleusercontent.com'
        })
    }

    const signinWithGoogle = async() => {
        try{
            const { idToken } = await GoogleSignin.signIn()
            console.log('Sign -> idtoken', idToken)

            const credential = auth.GoogleAuthProvider.credential(idToken)

            auth().signInWithCredential(credential)

            navigation.navigate('Profile') 
        }
        catch (err) {
            console.log('err', err)
        }
    }

    // const onLoginPress = () => {
    //     let data = {
    //         email : email,
    //         password: password
    //     }
    //     Axios.post(`${api}/login`, data, {
    //         timeout: 20000
    //     })
    //     .then((res) => {
    //         saveToken(res.data.token)
    //         navigation.navigate('Profile')
    //     })
    //     .catch((err) => {
    //         alert(err)
    //     })
    // }

    const onLoginPress = () => {
        return auth().signInWithEmailAndPassword(email, password)
        .then((res) => {
            navigation.reset({
                index: 0,
                routes: [{ name: 'Home' }]
            })
        })
        .catch((err) => {
            console.log(err)
        })
    }

    // const onLoginPress = () =>{
    //     navigation.reset({
    //         index: 0,
    //         routes: [{ name: 'Home' }]
    //     })

        // navigation.dispatch(
        //     CommonActions.reset({
        //         index: 0,
        //         routes: [{ name: 'Home'}]
        //     })
        // )
    //}

    const deleteToken = () => {
        Asynstorage.removeItem('intro')
        console.log("cek")
        // Asynstorage.getItem('token', (error, result) =>{
        //     if (result){
        //       Asynstorage.removeItem('intro')
        //       console.log('Terhapus')
              
        //     }
        //   })
    }

    const signInWithFingerPrint = () => {
        TouchID.authenticate('', config)
        .then(succes => {
            navigation.navigate('Profile')
        })
        .catch(error => {
            alert('Authentication Failed')
        })
    }

    return (
        <View style={{flex: 1}}>
            <View style={{width: '100%', height: 200, marginTop: 10, alignItems: 'center'}}>
                <Image source={require('./../../assets/images/logo.jpg')} style={{flex: 1}} />
            </View>
            <View style={{padding: 30}}>
                <Text>Username</Text>
                <TextInput value={email} onChangeText={(email) => setEmail(email)} underlineColorAndroid='#C6C6C6' placeholder='Username or Email' />
                <Text>Password</Text>
                <TextInput value={password} onChangeText={(password) => setPassword(password)} underlineColorAndroid='#C6C6C6' secureTextEntry placeholder='Password' style={{marginBottom: 10}}  />
                <Button onPress={() => onLoginPress()} color='#3EC6FF' title='LOGIN' />
                <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginVertical: 10}}>
                    <View style={{backgroundColor: 'black', height: 1, flex: 1}}></View>
                    <Text style={{marginHorizontal: 5}}>OR</Text>
                    <View style={{backgroundColor: 'black', height: 1, flex: 1}}></View>
                </View>
                <GoogleSigninButton onPress={() => signinWithGoogle()} style={{ width: '100%', height: 50}} size={GoogleSigninButton.Size.Wide} color={GoogleSigninButton.Color.Dark} />
                <View style={{marginTop: 5}}>
                    <Button color='#191970' title='Sign In With Finger Print' onPress={() => signInWithFingerPrint()} />
                </View>
                
            </View>
        </View>
    )
}

export default Login;