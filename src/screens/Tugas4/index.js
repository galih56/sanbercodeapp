import React, {useState, createContext} from 'react'
import { View, Text, Image, TextInput, TouchableOpacity, Button, ScrollView} from 'react-native'
import ListToDo from './ToDolist'

export const RootContext = createContext();
export default function index() {
    const [refreshing, setRefreshing] = React.useState(false);

  const [input, setInput] = useState('')
  const [todos, setTodos] = useState([])

  handleChangeInput = (value) => {
    setInput(value)
  }

  addTodo = () => {
    const date = new Date().getDate();
    const month = new Date().getMonth() + 1;
    const year = new Date().getFullYear();
    const today = date.toString()+'/'+month.toString()+'/'+year.toString()
    setTodos([...todos, {title: input, date: today}])
    setInput('')
  }

  deleteItem = (id) => {
      todos.splice(id, 1);
      setRefreshing(true) 
        setTimeout(() => {
          setRefreshing(false);
        }, 200)
      
    }
  
  
  return (
    <RootContext.Provider value={{
      input,
      todos,
      handleChangeInput,
      addTodo,
      deleteItem
    }}>
      <ListToDo />
    </RootContext.Provider>
  )
}
