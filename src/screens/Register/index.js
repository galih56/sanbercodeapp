import React, { useState } from 'react';
import { View, Text, Image, Alert, Modal, StatusBar, TextInput, TouchableOpacity, Button } from 'react-native';
// import styles from './style';
import { RNCamera } from 'react-native-camera';
import Icon from 'react-native-vector-icons/Feather';
import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';
import storage from '@react-native-firebase/storage';

function Register({ navigation }) {

    const [isVisible, setIsVisible] = useState(false)
    const [type, setType] = useState('back')
    const [photo, setPhoto] = useState(null)

    const toggleCamera = () => {
        setType(type === 'back' ? 'front' : 'back')
    }

    const takePicture = async() => {
        const options = { quality: 0.5, base64: true}
        if (camera) {
            const data = await camera.takePictureAsync(options)
            console.log("TakePicture -> data", data)
            setPhoto(data)
            setIsVisible(false)
        }
    }

    const uploadImage = (uri) => {
        const sessionID = new Date().getTime()
        return storage()
        .ref(`image/${sessionID}`)
        .putFile(uri)
        .then((response) => {
            alert('sukses')
        })
        .catch((error) => {
            alert('error')
        })
    }

    const renderCamera = () => {
        return (
            <Modal visible={isVisible} onRequestClose={() => setIsVisible(false)}>
                <View style={{ flex: 1 }}>
                    <RNCamera
                        style={{ flex: 1 }}
                        type={type}
                        ref={ref => {
                            camera = ref;
                        }}
                    >

                    <View style={{width: 40, height: 40, backgroundColor: '#FFFFFF', alignItems: 'center', justifyContent: 'center', borderRadius: 20, marginTop: 20, marginLeft: 20}}>
                        <TouchableOpacity onPress={() => toggleCamera()}>
                            <MaterialCommunity name='rotate-3d-variant' size={20}/>
                        </TouchableOpacity>
                    </View>
                    <View style={{flex: 1, alignItems: 'center'}}>
                        <View style={{width: '60%', height: 300, borderRadius: 100, borderColor: '#FFFFFF', borderWidth: 2}}></View>
                        <View style={{width: '60%', height: 140, borderColor: '#FFFFFF', borderWidth: 2, marginTop: 100}}></View>
                    </View>
                    <View style={{position: 'absolute', width: '100%', height: 70, bottom: 0, alignItems: 'center'}}>
                        <View style={{width: 60, height: 60, backgroundColor: '#FFFFFF', alignItems: 'center', justifyContent: 'center', borderRadius: 30}}>
                            <TouchableOpacity onPress={() => takePicture()}>
                                <Icon name='camera' size={30} />
                            </TouchableOpacity>
                        </View>
                    </View>
                    
                    
                    </RNCamera>
                </View>
            </Modal>
        )
    }

    return (
        
        <View>
            {renderCamera()}
            <View style={{width: '100%', height: '60%', backgroundColor: '#3EC6FF', alignItems: 'center', justifyContent: 'center'}}>
                <View style={{alignItems: 'center'}}>
                    <Image source={photo === null ? require('./../../assets/images/profile.jpg') : {uri: photo.uri}} style={{width: 100, height: 100, borderRadius: 50}} />
                    <TouchableOpacity onPress={() => setIsVisible(true)}>
                        <Text style={{fontSize: 20, fontWeight: 'bold', color: '#FFFFFF', marginTop: 15}}>Change picture</Text>
                    </TouchableOpacity>
                </View>
            </View>
            <View style={{flex: 1, alignItems: 'center'}}>
                <View elevation={3} style={{backgroundColor: '#FFFFFF', height: 300, width: '90%', top: -30, borderRadius: 10, padding: 20}}>
                <View style={{flex: 1}}>
                  <Text style={{fontWeight: 'bold', fontSize: 17}}>Nama</Text>
                  <TextInput placeholder='Nama' underlineColorAndroid={'gray'} />
                  <Text style={{fontWeight: 'bold', fontSize: 17}}>Email</Text>
                  <TextInput placeholder='Email' underlineColorAndroid={'gray'} />
                  <Text style={{fontWeight: 'bold', fontSize: 17}}>Password</Text>
                  <TextInput placeholder='Password' underlineColorAndroid={'gray'} />
                  <Button onPress={() => uploadImage(photo.uri)} color='#3EC6FF' title='REGISTER' />
                </View>
              </View>
            </View>
        </View>
    )
}

export default Register;