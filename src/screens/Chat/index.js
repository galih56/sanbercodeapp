import React, { useEffect, useState } from 'react'
import { View, Text } from 'react-native'
import auth from '@react-native-firebase/auth'
import database from '@react-native-firebase/database'
import { GiftedChat } from 'react-native-gifted-chat'
import BottomNavBar from './../../component/BottomNavBar'

export default function index( {navigation} ) {

    const [message, setMesaage] = useState([])
    const [user, setUser] = useState({})
    useEffect(() => {
        const user = auth().currentUser;
        setUser(user)
        getData()
        return () => {
            const db = database().ref('message')
            if(db) {
                db.off()
            }
        }
    }, [])

    const getData = () => {
        database().ref('message').limitToLast(20).on('child_added', snapshot => {
            const value = snapshot.val()
            setMesaage(previouseMessage => GiftedChat.append(previouseMessage, value))
        })
    }

    const onSend = (( message = []) => {
        for(let i = 0; i < message.length; i++) {
            database().ref('message').push({
                _id: message[i]._id,
                createdAt: database.ServerValue.TIMESTAMP,
                text: message[i].text,
                user: message[i].user
            })
        }
    })

    return (
        <View style={{flex: 1}}>
            <View style={{flex: 1, marginBottom: 70}}>
                <GiftedChat 
                messages={message}
                onSend={message => onSend(message)}
                user={{
                    _id: user.uid,
                    name: user.email,
                    avatar: require('../../assets/icons/avatar.png')
                }}
            />
            </View>
            
        <BottomNavBar navigation={navigation} />
        </View>
        
    )
}
