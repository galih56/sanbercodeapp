import React, { useEffect } from 'react';
import { View, Text, Image, StatusBar } from 'react-native';

import AppIntroSlider from 'react-native-app-intro-slider'; //import library atau module react-native-app-intro-slider
import Icon from 'react-native-vector-icons/Ionicons';
import Asynstorage from '@react-native-community/async-storage'

//data yang akan digunakan dalam onboarding
const slides = [
    {
        key: 1,
        title: 'Belajar Intensif',
        text: '4 pekan online, 5 hari sepekan, estimasi materi dan tugas 3-4 jam per hari',
        image: require('./../../assets/images/working-time.png')
    },
    {
        key: 2,
        title: 'Teknologi Populer',
        text: 'Menggunakan bahasa pemrograman populer',
        image: require('./../../assets/images/research.png'),
    },
    {
        key: 3,
        title: 'From Zero to Hero',
        text: 'Tidak ada syarat minimum skill, cocok untuk pemula',
        image: require('./../../assets/images/venture.png'),
    },
    {
        key: 4,
        title: 'Training Gratis',
        text: 'Kami membantu Anda mendapatkan pekerjaan / proyek',
        image: require('./../../assets/images/money-bag.png'),
    }
];

const Intro = ({ navigation }) => {

    useEffect(() => {
        Asynstorage.setItem('intro', '1')
    }, [])

    //menampilkan data slides kedalam renderItem
    const renderItem = ({ item }) => {
        return (
            <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                <Text style={{fontSize: 30, fontWeight: 'bold'}}>{item.title}</Text>
                <Image source={item.image} style={{height: 200, width: 200, marginVertical: 30}} />
                <Text style={{textAlign: 'center', marginHorizontal: 10, fontSize: 16, color: 'gray', fontWeight: 'bold'}}>{item.text}</Text>
            </View>
        );
    }

    //fungsi ketika onboarding ada di list terakhir atau screen terakhir / ketika button done di klik
    const onDone = () => {
        navigation.reset({
            index: 0,
            routes: [{ name: 'Login' }]
        })
    }

    //mengcustom tampilan button done
    const renderDoneButton = () => {
        return (
            <View style={{width: 50, height: 50, backgroundColor: '#191970', alignItems: 'center', justifyContent: 'center', borderRadius: 25}}>
                <Icon
                    name="md-checkmark"
                    color="rgba(255, 255, 255, .9)"
                    size={24}
                />
            </View>
        );
    };

    //mengcustom tampilan next button
    const renderNextButton = () => {
        return (
            <View style={{width: 50, height: 50, backgroundColor: '#191970', alignItems: 'center', justifyContent: 'center', borderRadius: 25}}>
                <Icon
                    name="arrow-forward"
                    color="rgba(255, 255, 255, .9)"
                    size={24}
                />
            </View>
        );
    };

    return (
        <View style={{flex: 1}}>
            <StatusBar barStyle="dark-content" backgroundColor="#ffffff" />
            <View style={{ flex: 1, backgroundColor: '#FFFFFF' }}>
                {/* merender atau menjalankan library react-native-app-intro-slider */}
                <AppIntroSlider
                    data={slides}
                    onDone={onDone}
                    renderItem={renderItem}
                    renderDoneButton={renderDoneButton}
                    renderNextButton={renderNextButton}
                    keyExtractor={(item, index) => index.toString()}
                    activeDotStyle={{ backgroundColor: '#191970' }}
                />
            </View>
        </View>
    )
}

export default Intro