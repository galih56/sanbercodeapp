import React, {useState, useEffect} from 'react'
import { View, Text, processColor } from 'react-native'
import { BarChart } from 'react-native-charts-wrapper'

export default function index({route}) {
    console.log(route)

    useEffect(() => {
        ChangeData()
        
      }, [])
    
      const [data, setData] = useState([
        {y:[100, 40], marker: ["React Native Dasar", "React Native Lanjutan"]},
        {y:[80, 60], marker: ["React Native Dasar", "React Native Lanjutan"]},
        {y:[40, 90], marker: ["React Native Dasar", "React Native Lanjutan"]},
        {y:[78, 45], marker: ["React Native Dasar", "React Native Lanjutan"]},
        {y:[67, 87], marker: ["React Native Dasar", "React Native Lanjutan"]},
        {y:[98, 32], marker: ["React Native Dasar", "React Native Lanjutan"]},
        {y:[150, 90], marker: ["React Native Dasar", "React Native Lanjutan"]}
      ])
    
     var newData = []
    
      const [legend, setLegend] = useState({
        enabled: false,
        textSize: 14,
        form: 'SQUARE',
        formSize: 14,
        xEntrySpace: 10,
        yEntrySpace: 5,
        formToTextSpace: 5,
        wordWrapEnabled: true,
        maxSizePercent: 0.5
    })
    
    const ChangeData = () =>{
      // for (var i in data) {
      //   //newData.push([{y:[data[i].y[0], data[i].y[1]]},{marker: [data[i].marker[0] + " " + data[i].y[0].toString(), data[i].marker[1] + " " + data[i].y[1].toString()]}])
      //   setNewData([...newData, [{y:[data[i].y[0], data[i].y[1]]}, {marker: [data[i].marker[0] + " " + data[i].y[0].toString(), data[i].marker[1] + " " + data[i].y[1].toString()]}]])
      // }
     
     // data.map((item) => {
        // var obj = {}
        // obj[item] = {y:[item.y[0], item.y[1]]},{marker:[item.marker[0], item.marker[1]]}
        // newData.push(obj)
        //newData.push({y:[item.y[0], item.y[1]]},{marker:[item.marker[0], item.marker[1]]})
     // })
      console.log(newData)
      console.log(data)
    }
    
    const [chart, setChart] = useState({
      data: {
          dataSets: [{
              values: data,
              label: '',
              config: {
                  colors: [processColor("#3EC6FF"), processColor("#088dc4")],
                  stackLabels: ['React Native Dasar', 'React Native Lanjutan'],
                  drawFilled: false,
                  drawValues: false,
              }
          }]
      }
    })
    
      const [xAxis, setXAxis] = useState({
          valueFormatter: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Agu', 'Sep', 'Oct', 'Nov', 'Des'],
          position: 'BOTTOM',
          drawAxisLine: true,
          drawGridLines: false,
          axisMinimum: -0.5,
          granularityEnabled: true,
          granularity: 1,
          axisMaximum: new Date().getMonth() + 0.5,
          spaceBetweenLabels: 0,
          labelRotationAngle: -45.0,
          limitLines: [{ limit: 115, lineColor: processColor('red'), lineWidth: 1 }]
      })
      const [yAxis, setYAxis] = useState({
          left: {
              axisMinimum: 0,
              labelCountForce: true,
              granularity: 5,
              granularityEnabled: true,
              drawGridLines: false
          },
          right: {
              axisMinimum: 0,
              labelCountForce: true,
              granularity: 5,
              granularityEnabled: true,
              enabled: false
          }
      })
    return (
        <BarChart
        style={{ flex: 1}}
        data={chart.data}
        yAxis={yAxis}
        xAxis={xAxis}
        pinchZoom={false}
        doubleTapToZoomEnabled={false}
        chartDescription={{text: ''}}
        legend={legend}
        marker={{
          enabled: true,
          markerColor: 'gray',
          textColor: '#FFFFFF',
          textSize: 14
        }}
      />
    )
}
