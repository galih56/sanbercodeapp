import React, { useEffect, useState } from 'react'
import { View, Text, Image, Button } from 'react-native'
import Axios from 'axios'
import Asynstorage from '@react-native-community/async-storage'
import { GoogleSignin } from '@react-native-community/google-signin'
import BottomNavBar from './../../component/BottomNavBar'
import auth from '@react-native-firebase/auth'

export default function Profile( {navigation} ) {

    const [userInfo, setUserInfo] = useState(null)
    useEffect(() => {
      async function getToken() {
        try {
          const token = await Asynstorage.getItem('token')
          return getVenue(token)
          console.log(token) 
        } catch(err) {
          console.log(err)
        }
      } 
      getToken()
      getCurrentUser()
    }, [])

    const getCurrentUser = async() => {
        try{
          const userInfo = await GoogleSignin.signInSilently()
          console.log('getCurrentUser -> ', userInfo)
          setUserInfo(userInfo)
        }
        catch (err) {
          console.log('getCurrenUser -> error : ', err)
        }
    }

    const getVenue = (token) => {
      Axios.get('https://mainbersama.demosanbercode.com/api/venues', {
        timeout: 20000,
        headers: {
          'Authorization' : 'Bearer' + token
        }
      })
      .then((res) => {
        console.log("Profile -> res", res)
      })
      .catch((err) => {
        console.log("Profile -> err". err)
      })
    }

    const onLogoutPress = async() => {
      try {
          await Asynstorage.getItem('token', (error, result) =>{
            if (result){
              Asynstorage.removeItem('token')
              console.log('Remove Token')
              
            }
          })
          const isSignedIn = await GoogleSignin.isSignedIn();
          if (isSignedIn){
              GoogleSignin.revokeAccess()
              GoogleSignin.signOut()
              console.log('LogOut Google')
          }
          auth().onAuthStateChanged(function(user) {
            if (user) {
                auth().signOut()
            } 
        })
          navigation.reset({
            index: 0,
            routes: [{ name: 'Login' }]
        })

      } catch(err) {
        console.log("Logout -> error : ",err)
      }
    }

      const Card = () => {
        return (
              <View elevation={3} style={{backgroundColor: '#FFFFFF', height: 260, width: '90%', top: -30, borderRadius: 10, padding: 20}}>
                <View style={{flex: 1}}>
                  <DataForm title='Tanggal Lahir' data='5 April 1998' />
                  <DataForm title='Jenis Kelamin' data='Laki-laki' />
                  <DataForm title='Hobi' data='Fotografi' />
                  <DataForm title='No. Telp' data='085731253969' />
                  <DataForm title='Email' data={userInfo && userInfo.user && userInfo.user.email} />
                  <Button onPress={() => onLogoutPress()} color='#3EC6FF' title='LOG OUT' />
                </View>
              </View>
        )
  }

  const Header = () => {
    return (
        <View style={{width: '100%', height: '60%', backgroundColor: '#3EC6FF', alignItems: 'center', justifyContent: 'center'}}>
            <View style={{alignItems: 'center'}}>
              <Image source={{uri : userInfo && userInfo.user && userInfo.user.photo}} style={{width: 100, height: 100, borderRadius: 50}} />
              <Text style={{fontSize: 20, fontWeight: 'bold', color: '#FFFFFF', marginTop: 15}}>{userInfo && userInfo.user && userInfo.user.name}</Text>
            </View>
          </View>
    )
  }


  const DataForm = (props) => {
    return (
      <View style={{flexDirection: 'row', justifyContent: 'space-between' , flex: 1}}>
          <Text style={{fontSize: 16, fontWeight: '500'}}>{props.title}</Text>
          <Text style={{fontSize: 16, fontWeight: '500'}}>{props.data}</Text>
      </View>
    )
  }

    return (
      <View style={{flex: 1}}>
          <View>
            <Header />
            <View style={{flex: 1, alignItems: 'center'}}>
              <Card />
            </View>
        </View>
        <BottomNavBar navigation={navigation} />
      </View>
    )
}

